/* CUSTOM ON LOAD FUNCTIONS */
function documentCustomLoad() {
    "use strict";
    console.log('Functions Correctly Loaded');

    var elementExists = document.getElementById('countdownTimer');
    if (elementExists) {
        var countDownDate = new Date(document.getElementById('countdownTimer').getAttribute('value')).getTime();
        // Update the count down every 1 second

        var x = setInterval(function() {

            // Get today's date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result
            document.getElementById('days-timer-label').innerHTML = days;
            document.getElementById("days-timer-path-remaining").setAttribute("stroke-dasharray", circleCustomCalc(days, 'days') + ' 283');
            document.getElementById('hours-timer-label').innerHTML = hours;
            document.getElementById("hours-timer-path-remaining").setAttribute("stroke-dasharray", circleCustomCalc(hours, 'hours') + ' 283');
            document.getElementById('minutes-timer-label').innerHTML = minutes;
            document.getElementById("minutes-timer-path-remaining").setAttribute("stroke-dasharray", circleCustomCalc(minutes, 'minutes') + ' 283');
            document.getElementById('seconds-timer-label').innerHTML = seconds;
            document.getElementById("seconds-timer-path-remaining").setAttribute("stroke-dasharray", circleCustomCalc(seconds, 'seconds') + ' 283');

            // If the count down is finished, write some text
            if (distance < 0) {
                clearInterval(x);
                document.getElementById('days-timer-label').innerHTML = 0;
                document.getElementById('hours-timer-label').innerHTML = 0;
                document.getElementById('minutes-timer-label').innerHTML = 0;
                document.getElementById('seconds-timer-label').innerHTML = 0;
            }
        }, 1000);
    }

    function circleCustomCalc(timeleft, intervalType) {
        if (intervalType === 'days') {
            var z = (timeleft.toFixed(0) * 100) / 30;
        }
        if (intervalType === 'hours') {
            var z = (timeleft.toFixed(0) * 100) / 24;
        }
        if (intervalType === 'minutes') {
            var z = (timeleft.toFixed(0) * 100) / 60;
        }
        if (intervalType === 'seconds') {
            var z = (timeleft.toFixed(0) * 100) / 60;
        }

        var circleDash = (283 * z) / 100;

        return circleDash.toFixed(0);
    }
}

document.addEventListener("DOMContentLoaded", documentCustomLoad, false);