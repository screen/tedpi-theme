var formSubscribe = document.getElementById('formSubscribe'),
    formBtn = document.getElementById('formBtn'),
    validForm = true;

function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

/* CUSTOM ON LOAD FUNCTIONS */
function documentFormCustomLoad() {
    "use strict";
    console.log('Form Functions Correctly Loaded');

    if (formSubscribe) {
        formBtn.addEventListener('click', function(e) {
            e.preventDefault();
            validForm = true;
            var elements = document.getElementsByClassName('form-email-input');
            var errorText = document.getElementById('errorEmail');
            if (elements[0].value == '') {
                errorText.classList.remove('d-none');
                validForm = false;
            } else {
                if (validateEmail(elements[0].value) == false) {
                    errorText.classList.remove('d-none');
                    validForm = false;
                } else {
                    errorText.classList.add('d-none');
                    validForm = true;
                }
            }

            if (validForm == true) {
                submitForm(validForm);
            }
        });
    }
}

function submitForm() {
    var emailForm = document.getElementsByClassName('form-email-input');
    var info = 'action=subscribe_contact&email_contact=' + emailForm[0].value;
    var elements = document.getElementsByClassName('loader-css');
    elements[0].classList.toggle("d-none");
    /* SEND AJAX */
    newRequest = new XMLHttpRequest();
    newRequest.open('POST', custom_admin_url.ajax_url, true);
    newRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    newRequest.onload = function() {
        var result = JSON.parse(newRequest.responseText);
        if (result.success == true) {
            elements[0].classList.toggle("d-none");
            window.location.href = custom_admin_url.thanks_url;
        } else {
            elements[0].classList.toggle("d-none");
            swal({
                title: 'Error on Request',
                text: 'Please try again later',
                icon: 'error',
                buttons: {
                    confirm: {
                        className: 'btn btn-danger',
                    },
                },
            });
        }
    };
    newRequest.send(info);
}


document.addEventListener("DOMContentLoaded", documentFormCustomLoad, false);