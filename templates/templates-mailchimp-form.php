<div class="mailchimp-form-container">
    <form id="formSubscribe" class="mailchimp-form-container-wrapper">
        <div class="input-group">
            <input type="email" class="form-control form-email-input" placeholder="<?php _e("Email Address", "tedpi"); ?>" aria-label="<?php _e("Email Address", "tedpi"); ?>" aria-describedby="button-addon2" autocomplete="email" tabindex="1" autocapitalize="off" autocorrect="off" autofocus="autofocus" />
            <div class="input-group-append">
                <button id="formBtn" class="btn btn-outline-secondary" type="button"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
            </div>
        </div>
        <small id="errorEmail" class="error-input text-danger d-none"><?php _e('Error: Email Address is Invalid', 'tedpi'); ?></small>
        <div class="loader-css d-none"></div>
    </form>
</div>