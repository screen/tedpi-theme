<div class="promo-countdown-container">
    <?php $countdown = get_post_meta(get_the_ID(), 'tpi_coming_date', true); ?>
    <?php if ($countdown != '') { ?>
    <input type="hidden" id="countdownTimer" value="<?php echo get_post_meta(get_the_ID(), 'tpi_coming_date', true); ?>">
    <?php } ?>
    <!-- DAYS -->
    <div class="days-timer base-timer">
        <svg class="base-timer__svg" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
            <g class="base-timer__circle">
                <circle class="base-timer__path-elapsed" cx="50" cy="50" r="45" />
                <path id="days-timer-path-remaining" stroke-dasharray="283" class="base-timer__path-remaining " d="M 50, 50 m -45, 0 a 45,45 0 1,0 90,0 a 45,45 0 1,0 -90,0"></path>
            </g>
        </svg>
        <span id="days-timer-label" class="base-timer__label"></span>
        <h4><?php _e('Days', 'tedpi'); ?></h4>
    </div>

    <!-- HOURS -->
    <div class="hours-timer base-timer">
        <svg class="base-timer__svg" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
            <g class="base-timer__circle">
                <circle class="base-timer__path-elapsed" cx="50" cy="50" r="45" />
                <path id="hours-timer-path-remaining" stroke-dasharray="283" class="base-timer__path-remaining " d="M 50, 50 m -45, 0 a 45,45 0 1,0 90,0 a 45,45 0 1,0 -90,0"></path>
            </g>
        </svg>
        <span id="hours-timer-label" class="base-timer__label"></span>
        <h4><?php _e('Hours', 'tedpi'); ?></h4>
    </div>
    <!-- MINUTES -->
    <div class="minutes-timer base-timer">
        <svg class="base-timer__svg" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
            <g class="base-timer__circle">
                <circle class="base-timer__path-elapsed" cx="50" cy="50" r="45" />
                <path id="minutes-timer-path-remaining" stroke-dasharray="283" class="base-timer__path-remaining " d="M 50, 50 m -45, 0 a 45,45 0 1,0 90,0 a 45,45 0 1,0 -90,0"></path>
            </g>
        </svg>
        <span id="minutes-timer-label" class="base-timer__label"></span>
        <h4><?php _e('Minutes', 'tedpi'); ?></h4>
    </div>
    <!-- SECONDS -->
    <div class="seconds-timer base-timer">
        <svg class="base-timer__svg" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
            <g class="base-timer__circle">
                <circle class="base-timer__path-elapsed" cx="50" cy="50" r="45" />
                <path id="seconds-timer-path-remaining" stroke-dasharray="283" class="base-timer__path-remaining " d="M 50, 50 m -45, 0 a 45,45 0 1,0 90,0 a 45,45 0 1,0 -90,0"></path>
            </g>
        </svg>
        <span id="seconds-timer-label" class="base-timer__label"></span>
        <h4><?php _e('Seconds', 'tedpi'); ?></h4>
    </div>
</div>