<?php
/**
* Template Name: Thanks Page
*
* @package startravel
* @subpackage tedpi-mk01-theme
* @since Mk. 1.0
*/
?>
<?php get_header('empty'); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <section class="coming-soon-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="landing-thanks-page-content col-12">
                        <a class="navbar-brand" href="<?php echo home_url('/');?>" title="<?php echo get_bloginfo('name'); ?>">
                            <?php $custom_logo_id = get_theme_mod( 'custom_logo' ); ?>
                            <?php $image = wp_get_attachment_image_src( $custom_logo_id , 'logo' ); ?>
                            <?php if (!empty($image)) { ?>
                            <img src="<?php echo $image[0];?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php echo get_bloginfo('name'); ?>" class="img-fluid img-logo" />
                            <?php } else { ?>
                            TEDPI
                            <?php } ?>
                        </a>
                        <svg class="thanks-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                            <circle class="path circle" fill="none" stroke="#73AC24" stroke-width="7" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1" />
                            <polyline class="path check" fill="none" stroke="#73AC24" stroke-width="7" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 " />
                        </svg>
                        <?php the_content(); ?>
                        <?php $button_link = get_post_meta(get_the_ID(), 'tpi_thanks_button_link', true); ?>
                        <?php if ($button_link != '') { ?>
                        <div class="button-thanks-container">
                            <?php $button_text= get_post_meta(get_the_ID(), 'tpi_thanks_button_text', true); ?>
                            <h5><?php echo get_post_meta(get_the_ID(), 'tpi_thanks_button_subtitle', true); ?></h5>
                            <a href="<?php echo $button_link; ?>" title="<?php echo $button_text; ?>" class="btn btn-md btn-thanks-action"><?php echo $button_text; ?></a>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer('empty'); ?>