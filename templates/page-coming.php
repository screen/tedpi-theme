<?php
/**
 * Template Name: Pagina Coming Soon
 *
 * @package tedpi
 * @subpackage tedpi-mk01-theme
 * @since Mk. 1.0
 */
?>
<?php get_header('empty'); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <section id="post-<?php the_ID(); ?>" class="coming-soon-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" role="article" itemscope itemtype="http://schema.org/BlogPosting">
            <div class="container">
                <div class="row">
                    <div class="section-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <a class="navbar-brand" href="<?php echo home_url('/'); ?>" title="<?php echo get_bloginfo('name'); ?>">
                            <?php $custom_logo_id = get_theme_mod('custom_logo'); ?>
                            <?php $image = wp_get_attachment_image_src($custom_logo_id, 'logo'); ?>
                            <?php if (!empty($image)) { ?>
                                <img src="<?php echo $image[0]; ?>" alt="<?php echo get_bloginfo('name'); ?>" class="img-fluid img-logo" />
                            <?php } else { ?>
                                Navbar
                            <?php } ?>
                        </a>
                        <?php the_content(); ?>
                        <?php echo get_template_part('templates/templates-mailchimp-form'); ?>
                        <div class="coming-soon-time">
                            <div class="coming-soon-time-wrapper">
                                <h3><?php echo get_post_meta(get_the_ID(), 'tpi_coming_title', true); ?></h3>
                                <?php echo get_template_part('templates/templates-countdown'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer('empty'); ?>