<div class="container-fluid p-0">
    <div class="row no-gutters">
        <div class="footer-coming col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <h5>&copy; 2020 Tedpi. All Rights Reserved | Developed by SMG | Digital Marketing Agency</h5>
        </div>
    </div>
</div>
<?php wp_footer() ?>
</body>

</html>