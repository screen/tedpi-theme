<?php
/* --------------------------------------------------------------
    1.- PRELAUNCH OPTIONS
-------------------------------------------------------------- */
$cmb_thanks_metabox = new_cmb2_box(array(
    'id'            => $prefix . 'thanks_metabox',
    'title'         => esc_html__('Thanks Page: Información Extra', 'tedpi'),
    'object_types'  => array('page'),
    'show_on'      => array('key' => 'page-template', 'value' => 'templates/page-thanks.php'),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
));

$cmb_thanks_metabox->add_field(array(
    'id'   => $prefix . 'thanks_button_subtitle',
    'name'      => esc_html__('Subtítulo anterior al boton de acción', 'tedpi'),
    'desc'      => esc_html__('Ingresa un sobretitulo que estara arriba del boton de acción', 'tedpi'),
    'type' => 'text'
));

$cmb_thanks_metabox->add_field(array(
    'id'   => $prefix . 'thanks_button_text',
    'name'      => esc_html__('Texto del boton de acción', 'tedpi'),
    'desc'      => esc_html__('Ingresa un texto descriptivo para el boton de acción', 'tedpi'),
    'type' => 'text'
));

$cmb_thanks_metabox->add_field(array(
    'id'   => $prefix . 'thanks_button_link',
    'name'      => esc_html__('Link URL del boton de acción', 'tedpi'),
    'desc'      => esc_html__('Ingresa un link descriptivo para el boton de acción', 'tedpi'),
    'type' => 'text_url'
));