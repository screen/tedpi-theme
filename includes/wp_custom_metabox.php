<?php
function ed_metabox_include_front_page($display, $meta_box)
{
    if (!isset($meta_box['show_on']['key'])) {
        return $display;
    }

    if ('front-page' !== $meta_box['show_on']['key']) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }

    if (!$post_id) {
        return false;
    }

    // Get ID of page set as front page, 0 if there isn't one
    $front_page = get_option('page_on_front');

    // there is a front page set and we're on it!
    return $post_id == $front_page;
}
add_filter('cmb2_show_on', 'ed_metabox_include_front_page', 10, 2);

function be_metabox_show_on_slug($display, $meta_box)
{
    if (!isset($meta_box['show_on']['key'], $meta_box['show_on']['value'])) {
        return $display;
    }

    if ('slug' !== $meta_box['show_on']['key']) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }

    if (!$post_id) {
        return $display;
    }

    $slug = get_post($post_id)->post_name;

    // See if there's a match
    return in_array($slug, (array) $meta_box['show_on']['value']);
}
add_filter('cmb2_show_on', 'be_metabox_show_on_slug', 10, 2);

add_action('cmb2_admin_init', 'tedpi_register_custom_metabox');
function tedpi_register_custom_metabox()
{
    $prefix = 'tpi_';

    /* PROMO */
    $cmb_coming_time = new_cmb2_box(array(
        'id'            => $prefix . 'time_metabox',
        'title'         => esc_html__('Coming Soon: Información Extra', 'tedpi'),
        'object_types'  => array('page'),
        'show_on'      => array('key' => 'page-template', 'value' => 'templates/page-coming.php'),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => false
    ));

    $cmb_coming_time->add_field(array(
        'id'   => $prefix . 'coming_title',
        'name'      => esc_html__('Título del Tiempo', 'tedpi'),
        'desc'      => esc_html__('Ingresa un Título Descriptivo para el Coming Soon', 'tedpi'),
        'type' => 'text'
    ));

    $cmb_coming_time->add_field(array(
        'id'   => $prefix . 'coming_date',
        'name'      => esc_html__('Fecha tope', 'tedpi'),
        'desc'      => esc_html__('Ingresa la fecha tope para el Coming Soon', 'tedpi'),
        'type' => 'text_date'
    ));

    require_once('custom-metaboxes-landing-thanks.php');
}
